package Test;

import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Step;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class MAISONETTE_test extends Browser{
	Report rpt = new Report();
	//public static WebDriver browser = BrowserFactory.getBrowser("FF","http://cct-live.qa.coc.ibmcloud.com/webapp/wcs/stores/servlet/en/circuitcityb2c");
	Browser br  = new Browser();

	@SuppressWarnings("static-access")
	@Parameters({"browser"})
	@Test(priority = 0, description="Invalid Login Scenario with wrong username and password.")
	@Description("Test Description: Login test with wrong username and wrong password.")
	@Step("Checking Login step.... ")
	@Attachment(value="Page ScreenShot", type="Screenshots/png")
	@Epic("Regression Test")
	public void executeAllTest()throws InterruptedException,Exception
	{	
		Thread.sleep(3000);
		
		 if(br.findTheElement("xpath=/html/body/header/ul[2]/li[2]/a").isDisplayed())
		 {
			 rpt.createTest("Check Login", "Displayed");
			 //rpt.Info("Expected Message "+Expected+" Is Matching with "+Actual +" Message");
			 rpt.Pass("Login Displayed");
			 rpt.Category("MAISONETTE-CheckLogin");
			 String path = rpt.CaptureScreen(browser, "ValidMessage");
			 rpt.imgPathPass(path);
			 Thread.sleep(2000);
			 br.click("xpath=/html/body/header/ul[2]/li[2]/a");
		 }
		
		 Thread.sleep(2000);
			br.click("xpath=//*[@id='new_spree_user']/div/div[6]/a");
			Thread.sleep(2000);
			br.sendKeys("xpath=//*[@id='spree_user_first_name']", "Test");
			br.sendKeys("xpath=//*[@id='spree_user_last_name']", "User");
			br.sendKeys("xpath=//*[@id='spree_user_email']", "TestUser@email.com");
			br.sendKeys("xpath=//*[@id='r-password']", "password");
			br.sendKeys("xpath=//*[@id='r-confirm-password']", "ConfirmPwd");
			
			System.out.println("Inside executeAllTest");
	}
}
