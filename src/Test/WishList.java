package Test;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class WishList extends Browser
{

	Report rpt = new Report();
	Browser br  = new Browser();

	@SuppressWarnings("static-access")
	@Parameters({"browser"})
	@Test
	public void executeAllTest()throws Exception
	{
		    Thread.sleep(6000);
		    click("xpath=//*[@id='Header_GlobalLogin_signInQuickLink']");
		    Thread.sleep(6000);
			click("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonId_In_Logon_1']");
			sendKeys("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonId_In_Logon_1']", "zainab.firdaus@royalcyber.com");
			Thread.sleep(6000);
			click("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonPassword_In_Logon_1']");
			sendKeys("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonPassword_In_Logon_1']", "zain@1926");
			click("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_links_2']");
			Thread.sleep(5000);
			click("xpath=html/body/div[2]/div[2]/div[2]/div[2]/div[4]/ul/li[3]/div/div/div/div/div/div[2]/div[3]/div[2]/a"); //my acc
		//accountSummary();
		accountPage();
		wishList();
		wishListDisplay();
		newWishlist();
		changeWishlist();
		deleteWishlist();
		removeProduct();
		emailWishlist();
		addProduct();
		verifyWishlist();
	}
	
	public void accountSummary()throws Exception
	{
		String Actual  = findTheElement("xpath=//*[@id='container_MyAccountDisplayB2B']/div/div[2]/div/div[1]/div[1]/div").getText();
		Boolean welcmMsg = findTheElement("xpath=//*[@id='container_MyAccountDisplayB2B']/div/div[2]/div/div[1]/div[1]/div").isDisplayed();
		String Expected="GOOD Morning";
	     if(Actual == Expected)
	                   {
		                
	    	            rpt.createTest("CC-Address Book-Check details", "Message"+Actual+" Displayed For Welcome Message:");
	    	            rpt.Info("Expected Message"+Expected+"Is Matching with "+Actual +"Message For Welcome Message:");
	    	            rpt.Pass("Message"+Expected+" Displayed- For Welcome Message:");
	    	            rpt.Category("CC_Address Book_Check_Details");
	      		        String path = rpt.CaptureScreen(browser, "ValidMessage");
	      		        rpt.imgPathPass(path);
	    	            
	                    }
	                      else
	                    {
		                 
	          	          rpt.createTest("CC-Address Book-Check details", "Message"+Actual+" Displayed- For Welcome Message:");
	          	          rpt.Info("Expected Message"+Expected+ "Not Matching with "+Actual +"Message");
	          	          rpt.Fail("Message"+Expected+" NOT Displayed- For Welcome Message:");  
	          	          rpt.Category("CC_Address Book_Check_Details");
	      		          String path = rpt.CaptureScreen(browser, "InvalidMessage");
	      		          rpt.imgPathFail(path);
	                     }
	     
	     
	}
	
	public void accountPage()throws Exception
	{
		Map<String,String> ids= new HashMap<String,String>();
		 ids.put("Settings", "section_button_settings");
		 ids.put("Wishlist", "section_button_wishlists");
		 ids.put("Orders", "section_button_orders");
		 ids.put("Gifts", "section_button_coupons");
		 for(Map.Entry<String, String> id : ids.entrySet() )
		 {
			 if(findTheElement("xpath=//*[@id='"+id.getValue()+"']").isDisplayed())
			 {
		         
		         rpt.createTest("CC-WishList-Check Account Section", "Message :"+id.getKey()+" Displayed - For Account Section");
		         rpt.Pass("Message :"+id.getKey()+" Displayed - For Account Section");
		         rpt.Category("CC_WishList-Check Account Section");
		         String path = rpt.CaptureScreen(browser, "ValidMessage");
		         rpt.imgPathPass(path);
 
			 }
			 else
			 {
		         rpt.createTest("CC-WishList-Check Account Section", "Message :"+id.getKey()+"is NOT Displayed - For Account Section");
		         rpt.Pass("Message :"+id.getKey()+" NOT Displayed - For Account Section");
		         rpt.Category("CC_WishList-Check Account Section");
		         String path = rpt.CaptureScreen(browser, "InvalidMessage");
		         rpt.imgPathFail(path);
			 }
		 }
	}
	
	public void wishList()throws Exception
	{
		
		Map<String,String> ids= new HashMap<String,String>();
		 ids.put("Personal Wishlist", "WC_MyAccountSidebarDisplayf_links_4");
		 
		 for(Map.Entry<String, String> id : ids.entrySet() )
		 {
			 if(findTheElement("xpath=//*[@id='"+id.getValue()+"']").isDisplayed())
			 {
		         
		         rpt.createTest("CC-WishList-Check Account Section", "Message :"+id.getKey()+" Displayed - For Account Section");
		         rpt.Pass("Message :"+id.getKey()+" Displayed - For Account Section");
		         rpt.Category("CC_WishList-Check Account Section");
		         String path = rpt.CaptureScreen(browser, "ValidMessage");
		         rpt.imgPathPass(path);

			 }
			 else
			 {
		         rpt.createTest("CC-WishList-Check Account Section", "Message :"+id.getKey()+"is NOT Displayed - For Account Section");
		         rpt.Pass("Message :"+id.getKey()+" NOT Displayed - For Account Section");
		         rpt.Category("CC_WishList-Check Account Section");
		         String path = rpt.CaptureScreen(browser, "InvalidMessage");
		         rpt.imgPathFail(path);
			 }
		 }
	}
	
	public void wishListDisplay()throws Exception
	{
		//click("xpath=//*[@id='WC_MyAccountSidebarDisplayf_links_4']"); //personal wishlist
		if(findTheElement("xpath=//*[@id='WC_MyAccountSidebarDisplayf_links_4']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify WishList", " Personal WishList Displayed - For Verify WishList");
	        rpt.Pass(" Personal WishList  Displayed - For Verify WishList");	
	        rpt.Category("CC_WishList - Verify WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='WC_MyAccountSidebarDisplayf_links_4']"); //personal wishlist

		}
		else
		{
			rpt.createTest("CC - WishList - Verify WishList", " Personal WishList  NOT Displayed - For Verify WishList");
		    rpt.Fail(" Personal WishList  NOT Displayed - For Verify WishList");	
		    rpt.Category("CC_WishList - Verify WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
	}
	
	public void newWishlist()throws Exception
	{
		//click("xpath=//*[@id='WC_MyAccountSidebarDisplayf_links_4']");
		if(findTheElement("xpath=//*[@id='WC_MyAccountSidebarDisplayf_links_4']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify WishList", " Personal WishList Displayed - For Verify New WishList");
	        rpt.Pass(" Personal WishList  Displayed - For Verify New WishList");	
	        rpt.Category("CC_WishList - Verify WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='WC_MyAccountSidebarDisplayf_links_4']"); //personal wishlist

		}
		else
		{
			rpt.createTest("CC - WishList - Verify WishList", " Personal WishList  NOT Displayed - For Verify New WishList");
		    rpt.Fail(" Personal WishList  NOT Displayed - For Verify New  WishList");	
		    rpt.Category("CC_WishList - Verify WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		Thread.sleep(5000);
		click("xpath=//*[@id='multipleWishlistController_link_create']"); 
		Thread.sleep(5000);
		click("xpath=//*[@id='newListName']");
		//sendKeys("xpath=//*[@id='newListName']","dress"); 
		if(findTheElement("xpath=//*[@id='newListName']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify WishList", " New WishList Displayed - For Verify New WishList");
	        rpt.Pass(" New WishList  Displayed - For Verify New WishList");	
	        rpt.Category("CC_WishList - Verify WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		sendKeys("xpath=//*[@id='newListName']","zainab");

		}
		else
		{
			rpt.createTest("CC - WishList - Verify WishList", " New WishList NOT Displayed - For Verify New WishList");
		    rpt.Fail(" New WishList  NOT Displayed - For Verify New  WishList");	
		    rpt.Category("CC_WishList - Verify WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
	        
		}
		
		//click("xpath=//*[@id='editShoppingListPopup_div_edit_save']"); //save
		if(findTheElement("xpath=//*[@id='CreateShoppingListPopup_div_create_save']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify WishList", " Save Button Displayed - For Verify New WishList");
	        rpt.Pass(" Save Button Displayed - For Verify New WishList");	
	        rpt.Category("CC_WishList - Verify WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='CreateShoppingListPopup_div_create_save']"); //save

		}
		else
		{
			rpt.createTest("CC - WishList - Verify WishList", " Save Button NOT Displayed - For Verify New WishList");
		    rpt.Fail(" Save Button NOT Displayed - For Verify New  WishList");	
		    rpt.Category("CC_WishList - Verify WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
	}
	
	public void changeWishlist()throws Exception
	{
		
		//click("xpath=//*[@id='multipleWishlistController_link_edit']");//rename
		if(findTheElement("xpath=//*[@id='multipleWishlistController_link_edit']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Change WishList", " Rename Button Displayed - For Verify Change WishList");
	        rpt.Pass(" Rename Button Displayed - For Verify Change WishList");	
	        rpt.Category("CC_WishList - Verify Change WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='multipleWishlistController_link_edit']");//rename

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Change WishList", " Rename Button NOT Displayed - For Verify Change WishList");
		    rpt.Fail(" Rename Button NOT Displayed - For Verify Change  WishList");	
		    rpt.Category("CC_WishList - Verify Change WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		click("xpath=//*[@id='editListName']");
		//sendKeys("xpath=//*[@id='editListName']","test");
		if(findTheElement("xpath=//*[@id='editListName']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Change WishList", " WishList Displayed - For Verify Change WishList");
	        rpt.Pass(" WishList Displayed - For Verify Change WishList");	
	        rpt.Category("CC_WishList - Verify Change WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		sendKeys("xpath=//*[@id='editListName']","test");

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Change WishList", " WishList NOT Displayed - For Verify Change WishList");
		    rpt.Fail(" WishList NOT Displayed - For Verify Change WishList");	
		    rpt.Category("CC_WishList - Verify Change WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		//click("xpath=//*[@id='editShoppingListPopup_div_edit_save']"); //save
		if(findTheElement("xpath=//*[@id='editShoppingListPopup_div_edit_save']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Change WishList", " Save Button Displayed - For Verify Change WishList");
	        rpt.Pass(" Save Button Displayed - For Verify Change WishList");	
	        rpt.Category("CC_WishList - Verify Change WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='editShoppingListPopup_div_edit_save']"); //save

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Change WishList", " Save Button NOT Displayed - For Verify Change WishList");
		    rpt.Fail(" Save Button NOT Displayed - For Verify Change WishList");	
		    rpt.Category("CC_WishList - Verify Change WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		Thread.sleep(5000);
		click("xpath=html/body/div[8]/div[2]/div/div[3]/div/div/div/div[2]/div[1]/a/div[2]"); //continue shopping
	}
	
	public void deleteWishlist()throws Exception
	{
		//click("xpath=//*[@id='multipleWishlistController_link_delete']"); //delete
		if(findTheElement("xpath=//*[@id='multipleWishlistController_link_delete']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Delete WishList", " Delete Button Displayed - For Verify Delete WishList");
	        rpt.Pass(" Delete Button Displayed - For Verify Change WishList");	
	        rpt.Category("CC_WishList - Verify Delete WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='multipleWishlistController_link_delete']"); //delete

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Delete WishList", " Delete Button NOT Displayed - For Delete Change WishList");
		    rpt.Fail(" Delete Button NOT Displayed - For Verify Delete WishList");	
		    rpt.Category("CC_WishList - Verify Delete WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		//click("xpath=//*[@id='deleteShoppingListPopup_div_delete_yes']");
		if(findTheElement("xpath=//*[@id='deleteShoppingListPopup_div_delete_yes']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Delete WishList", " Yes Button Displayed - For Verify Delete WishList");
	        rpt.Pass(" Yes Button Displayed - For Verify Change WishList");	
	        rpt.Category("CC_WishList - Verify Delete WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='deleteShoppingListPopup_div_delete_yes']");

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Delete WishList", " Yes Button NOT Displayed - For Delete Change WishList");
		    rpt.Fail(" Yes Button NOT Displayed - For Verify Delete WishList");	
		    rpt.Category("CC_WishList - Verify Delete WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		click("xpath=//*[@id='shoppingListCreateSuccessPopup_continue_shopping']/div[2]");
	}
	
	public void removeProduct()throws Exception
	{
		//click("xpath=//*[@id='WC_MyAccountSidebarDisplayf_links_4']");
		if(findTheElement("xpath=//*[@id='WC_MyAccountSidebarDisplayf_links_4']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Remove WishList", " Personal WishList Displayed - For Verify Remove WishList");
	        rpt.Pass(" Personal WishList  Displayed - For Verify Remove WishList");	
	        rpt.Category("CC_WishList - Verify Remove WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='WC_MyAccountSidebarDisplayf_links_4']"); //personal wishlist

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Remove WishList", " Personal WishList  NOT Displayed - For Verify Remove WishList");
		    rpt.Fail(" Personal WishList  NOT Displayed - For Verify Remove  WishList");	
		    rpt.Category("CC_WishList - Verify Remove WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		//click("xpath=//*[@id='WC_CatalogEntryDBThumbnailDisplayJSPF_3074457345616677177_links_14']");
		if(findTheElement("xpath=//*[@id='WC_CatalogEntryDBThumbnailDisplayJSPF_3074457345616677177_links_14']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Remove WishList", " Remove Button Displayed - For Verify Remove WishList");
	        rpt.Pass(" Remove Button Displayed - For Verify Remove WishList");	
	        rpt.Category("CC_WishList - Verify Remove WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='WC_CatalogEntryDBThumbnailDisplayJSPF_3074457345616677177_links_14']");

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Remove WishList", " Remove Button NOT Displayed - For Verify Remove WishList");
		    rpt.Fail(" Remove Button NOT Displayed - For Verify Remove  WishList");	
		    rpt.Category("CC_WishList - Verify Remove WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		click("xpath=//*[@id='shoppingListCreateSuccessPopup_continue_shopping']");
	}
	
	public void emailWishlist()throws Exception
	{
		//click("xpath=//*[@id='multipleWishlistController_link_delete']"); //share
		if(findTheElement("xpath=//*[@id='multipleWishlistController_link_delete']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Email WishList", " Share Button Displayed - For Verify Email WishList");
	        rpt.Pass(" Share Button Displayed - For Verify Email WishList");	
	        rpt.Category("CC_WishList - Verify Email WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='multipleWishlistController_link_delete']");

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Email WishList", " Share Button NOT Displayed - For Verify Email WishList");
		    rpt.Fail(" Share Button NOT Displayed - For Verify Email  WishList");	
		    rpt.Category("CC_WishList - Verify Email WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		click("xpath=//*[@id='SendWishListForm_Recipient_Email']");
		//sendKeys("xpath=//*[@id='SendWishListForm_Recipient_Email']","zainab.f@royalcyber.com");
		if(findTheElement("xpath=//*[@id='SendWishListForm_Recipient_Email']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Email WishList", " Recipient Email Displayed - For Verify Email WishList");
	        rpt.Pass(" Recipient Email Displayed - For Verify Email WishList");	
	        rpt.Category("CC_WishList - Verify Email WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		sendKeys("xpath=//*[@id='SendWishListForm_Recipient_Email']","zainab.f@royalcyber.com");

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Email WishList", " Recipient Email NOT Displayed - For Verify Email WishList");
		    rpt.Fail(" Recipient Email NOT Displayed - For Verify Email  WishList");	
		    rpt.Category("CC_WishList - Verify Email WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		click("xpath=//*[@id='SendWishListForm_Recipient_Name']");
		//sendKeys("xpath=//*[@id='SendWishListForm_Recipient_Name']","Zainab");
		if(findTheElement("xpath=//*[@id='SendWishListForm_Recipient_Email']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Email WishList", " Recipient Name Displayed - For Verify Email WishList");
	        rpt.Pass(" Recipient Name Displayed - For Verify Email WishList");	
	        rpt.Category("CC_WishList - Verify Email WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		sendKeys("xpath=//*[@id='SendWishListForm_Recipient_Name']","Zainab");

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Email WishList", " Recipient Name NOT Displayed - For Verify Email WishList");
		    rpt.Fail(" Recipient Name NOT Displayed - For Verify Email  WishList");	
		    rpt.Category("CC_WishList - Verify Email WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		click("xpath=//*[@id='SendWishListForm_Sender_Email']");
		//sendKeys("xpath=//*[@id='SendWishListForm_Sender_Email']","zfirdaus95@gmail.com");
		if(findTheElement("xpath=//*[@id='SendWishListForm_Sender_Email']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Email WishList", " Sender Email Displayed - For Verify Email WishList");
	        rpt.Pass(" Sender Email Displayed - For Verify Email WishList");	
	        rpt.Category("CC_WishList - Verify Email WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		sendKeys("xpath=//*[@id='SendWishListForm_Sender_Email']","zfirdaus95@gmail.com");

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Email WishList", " Sender Email NOT Displayed - For Verify Email WishList");
		    rpt.Fail(" Sender Email NOT Displayed - For Verify Email  WishList");	
		    rpt.Category("CC_WishList - Verify Email WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		click("xpath=//*[@id='SendWishListForm_Sender_Name']");
		//sendKeys("xpath=//*[@id='SendWishListForm_Sender_Name']","Firdaus");
		if(findTheElement("xpath=//*[@id='SendWishListForm_Sender_Name']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Email WishList", " Sender Name Displayed - For Verify Email WishList");
	        rpt.Pass(" Sender Name Displayed - For Verify Email WishList");	
	        rpt.Category("CC_WishList - Verify Email WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		sendKeys("xpath=//*[@id='SendWishListForm_Sender_Name']","Firdaus");

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Email WishList", " Sender Name NOT Displayed - For Verify Email WishList");
		    rpt.Fail(" Sender Name NOT Displayed - For Verify Email  WishList");	
		    rpt.Category("CC_WishList - Verify Email WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		//click("xpath=//*[@id='WC_WishListDisplay_links_1']/div[2]");
		if(findTheElement("xpath=//*[@id='WC_WishListDisplay_links_1']/div[2]").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Email WishList", " Save Button Displayed - For Verify Email WishList");
	        rpt.Pass(" Save Button Displayed - For Verify Email WishList");	
	        rpt.Category("CC_WishList - Verify Email WishList");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='WC_WishListDisplay_links_1']/div[2]");

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Email WishList", " Save Button NOT Displayed - For Verify Email WishList");
		    rpt.Fail(" Save Button NOT Displayed - For Verify Email  WishList");	
		    rpt.Category("CC_WishList - Verify Email WishList");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
	}
	
	public void addProduct()throws Exception
	{
		click("xpath=//*[@id='shoppingListCreateSuccessPopup_continue_shopping']/div[2]"); //continue shopping
		//click("xpath=//*[@id='listViewAdd2Cart_3074457345616727849']"); //buy now
		if(findTheElement("xpath=//*[@id='listViewAdd2Cart_3074457345616727849']").isDisplayed())
		{		
		    rpt.createTest("CC - WishList - Verify Add Product", " Buy Now Displayed - For Verify Add Product");
	        rpt.Pass(" Buy Now Displayed - For Verify Add Product");	
	        rpt.Category("CC_WishList - Verify Add Product");
           String path = rpt.CaptureScreen(browser, "ValidMessage");
            rpt.imgPathPass(path);
    		click("xpath=//*[@id='listViewAdd2Cart_3074457345616727849']"); //buy now

		}
		else
		{
			rpt.createTest("CC - WishList - Verify Add Product", " Buy Now NOT Displayed - For Verify Add Product");
		    rpt.Fail(" Buy Now NOT Displayed - For Verify Add Product");	
		    rpt.Category("CC_WishList - Verify Add Product");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
		}
		click("xpath=//*[@id='WC_BreadCrumbTrailDisplay_links_2a']/div[2]"); //continue shopping
	}
	
	public void verifyWishlist()throws Exception
	{
		//UI 
	}
	
}
