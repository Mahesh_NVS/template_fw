package Test;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class productComparison extends Browser
{
	Report rpt = new Report();
	Browser br  = new Browser();

	@SuppressWarnings("static-access")
	@Parameters({"browser"})
	@Test
	
	public void executeAll() throws Exception
	{
		//verifyProduct();
    	//customerView();
		//addedProduct();
//		addToCart();
//		compareProduct();
//		compareCategory();
//		verifyCompare();
//		shareProduct();
//		verifyEmail();
//		verifyPrint();
//		verifyRemove();
//		verifySearch();
	}
	//CC_CP_01
	public void verifyProduct() throws Exception
	{
		click("xpath=//*[@id='SimpleSearchForm_SearchTerm']");
		Thread.sleep(3000);
		sendKeys("xpath=//*[@id='SimpleSearchForm_SearchTerm']","Lenovo 20EN001SUS TS P50 E3 16GB 256GB");
		Thread.sleep(5000);
		click("xpath=//*[@id='autoSelectOption_0']/div/img");
		Thread.sleep(5000);
	    Robot r = new Robot();
	    /*for(int i=0;i<4;i++){*/
//	    r.keyPress(KeyEvent.VK_DOWN);
//	    r.keyRelease(KeyEvent.VK_DOWN);
	    
	    
	    WebElement scroll = findTheElement("xpath=//*[@id='compare_3074457345616683129']/label");
	    scroll.sendKeys(Keys.PAGE_DOWN);
	
	  //*[@id='compare_3074457345616721769']/label

	   /* Thread.sleep(5000);
	    JavascriptExecutor jse = (JavascriptExecutor)driver;
	    jse.executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(3000);*/
		click("xpath=//*[@id='compare_3074457345616683129']/label"); //compare
		
		Thread.sleep(3000);
		//Robot r = new Robot();
		Thread.sleep(3000);
		
		/*Screen s  = new Screen();
		Thread.sleep(6000);
		Pattern img=new Pattern("C:\\Users\\user\\workspace\\CC\\sikuli images\\compare_cc.png");
		Thread.sleep(5000);
		s.click(img);*/
	}
	
	public void customerView() throws Exception
	{
		click("xpath=//*[@id='allDepartmentsButton']/span"); //product
		Thread.sleep(3000);
		click("xpath=//*[@id='departmentLink_3074457345616682178_alt']"); 
		Thread.sleep(3000);
		Robot r = new Robot();
//		for(int i=0; i<6; i++) {
//	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
//	    }
		WebElement scroll1 = findTheElement("xpath=//*[@id='WC_CatalogEntryDBThumbnailDisplayJSPF_3074457345616727788_link_9b']");
		scroll1.sendKeys(Keys.PAGE_DOWN);
		click("xpath=//*[@id='WC_CatalogEntryDBThumbnailDisplayJSPF_3074457345616727788_link_9b']");
		Thread.sleep(6000);
				
//		 WebElement scroll = findTheElement("xpath=//*[@id='compare_3074457345616707434']/label");
		WebElement scroll = findTheElement("xpath=//*[@id='compare_3074457345616721769']");
		  scroll.sendKeys(Keys.PAGE_DOWN);
		  
		  
		
		  

		  
		
		/*r.keyPress(KeyEvent.VK_PAGE_DOWN);
		r.keyRelease(KeyEvent.VK_PAGE_DOWN);
	    r.keyPress(KeyEvent.VK_PAGE_DOWN);
		r.keyRelease(KeyEvent.VK_PAGE_DOWN);*/
		Thread.sleep(6000);
		click("xpath=//*[@id='compare_3074457345616721769']");
	}
	
	public void addedProduct()throws Exception
	{
		click("xpath=//*[@id='allDepartmentsButton']/span"); //product
		Thread.sleep(3000);
		click("xpath=//*[@id='departmentLink_3074457345616682178_alt']"); 
		Thread.sleep(3000);
		Robot r = new Robot();
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		
		click("xpath=//*[@id='WC_CatalogEntryDBThumbnailDisplayJSPF_3074457345616727788_link_9b']");
		Thread.sleep(4000);
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='compare_3074457345616707434']/label");
		click("xpath=//*[@id='compare_3074457345616734525']/label");
	    click("xpath=//*[@id='compareButton_3074457345616734525']"); //compare
	    
	}
	public void addToCart()throws Exception
	{
		click("xpath=//*[@id='comparePageAdd2Cart_1']"); //add to cart
	}
    public void compareProduct()throws Exception
    {
    	click("xpath=//*[@id='MiniShopCartCloseButton_2']"); //close
    	click("xpath=//*[@id='allDepartmentsButton']/span"); //product
		Thread.sleep(3000);
		click("xpath=//*[@id='departmentLink_3074457345616682178_alt']"); 
		Thread.sleep(3000);
		Robot r = new Robot();
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='WC_CatalogEntryDBThumbnailDisplayJSPF_3074457345616727788_link_9b']");
		Thread.sleep(4000);
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='compare_3074457345616707434']/label");
		click("xpath=//*[@id='compare_3074457345616734525']/label");
		click("xpath=//*[@id='compare_3074457345616734523']/label");
		click("xpath=//*[@id='compare_3074457345616727948']/label");
	    click("xpath=//*[@id='compareButton_3074457345616734525']"); //compare
    	
    }
    
    //CC_CP_07
    public void compareCategory()throws Exception
    {
    	click("xpath=//*[@id='SimpleSearchForm_SearchTerm']");
		Thread.sleep(3000);
		sendKeys("xpath=//*[@id='SimpleSearchForm_SearchTerm']","Samsung");
		Thread.sleep(3000);
		click("xpath=//*[@id='autoSelectOption_4']/div/img");
		Robot r = new Robot();
		for(int i=0; i<15; i++) 
		{
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='compare_3074457345616688890']/label"); //dell product
		click("xpath=//*[@id='compare_3074457345616683133']/label"); //lenovo
		click("xpath=//*[@id='compareButton_3074457345616683133']");
		
    }
    //CC_CP_08 //to ask
    public void verifyCompare()throws Exception
    {
    	click("xpath=//*[@id='allDepartmentsButton']/span"); //product
		Thread.sleep(3000);
		click("xpath=//*[@id='departmentLink_3074457345616682169_alt']"); 
		Thread.sleep(3000);
		Robot r = new Robot();
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='catalogEntry_img3074457345616727848']");
		Thread.sleep(4000);
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='compare_3074457345616723385']/label");
		click("xpath=//*[@id='compare_3074457345616727948']/label");
		click("xpath=//*[@id='compareButton_3074457345616727948']"); //compare
		browser.navigate().back();
		
    }
    
    public void shareProduct()throws Exception
    {
    	click("xpath=//*[@id='allDepartmentsButton']/span"); //product
		Thread.sleep(3000);
		click("xpath=//*[@id='departmentLink_3074457345616682169_alt']"); 
		Thread.sleep(3000);
		Robot r = new Robot();
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='catalogEntry_img3074457345616727848']");
		Thread.sleep(4000);
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='compare_3074457345616723385']/label");
		click("xpath=//*[@id='compare_3074457345616727948']/label");
		click("xpath=//*[@id='compareButton_3074457345616727948']"); //compare
		
		click("xpath=//*[@id='sticky']/div/div[1]/div/a"); //share
    }
    
    public void verifyEmail()throws Exception
    {
    	click("xpath=//*[@id='allDepartmentsButton']/span"); //product
		Thread.sleep(3000);
		click("xpath=//*[@id='departmentLink_3074457345616682169_alt']"); 
		Thread.sleep(3000);
		Robot r = new Robot();
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='catalogEntry_img3074457345616727848']");
		Thread.sleep(4000);
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='compare_3074457345616723385']/label");
		click("xpath=//*[@id='compare_3074457345616727948']/label");
		click("xpath=//*[@id='compareButton_3074457345616727948']"); //compare
		
		click("xpath=//*[@id='sticky']/div/div[1]/div/div[2]/a[1]/img"); //email
		sendKeys("xpath=//*[@id='shareCompareResult']/input[4]","test");
		sendKeys("xpath=//*[@id='shareCompareResult']/input[5]","test");
		sendKeys("xpath=//*[@id='shareCompareResult']/input[6]","zainab.f@royalcyber.com");
		click("xpath=/html/body/div[13]/div[3]/div/button[1]");
    }
    public void verifyPrint()throws Exception
    {
    	click("xpath=//*[@id='allDepartmentsButton']/span"); //product
		Thread.sleep(3000);
		click("xpath=//*[@id='departmentLink_3074457345616682169_alt']"); 
		Thread.sleep(3000);
		Robot r = new Robot();
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='catalogEntry_img3074457345616727848']");
		Thread.sleep(4000);
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='compare_3074457345616723385']/label");
		click("xpath=//*[@id='compare_3074457345616727948']/label");
		click("xpath=//*[@id='compareButton_3074457345616727948']"); //compare
		click("xpath=//*[@id='sticky']/div/div[1]/div/div[2]/a[2]"); //print
		click("xpath=//*[@id='print-header']/div/button[2]");
    }
    
    public void verifyRemove()throws Exception
    {
    	click("xpath=//*[@id='allDepartmentsButton']/span"); //product
		Thread.sleep(3000);
		click("xpath=//*[@id='departmentLink_3074457345616682169_alt']"); 
		Thread.sleep(3000);
		Robot r = new Robot();
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='catalogEntry_img3074457345616727848']");
		Thread.sleep(4000);
		for(int i=0; i<6; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='compare_3074457345616723385']/label");
		click("xpath=//*[@id='compare_3074457345616727948']/label");
		click("xpath=//*[@id='compareButton_3074457345616727948']"); //compare
		
		click("xpath=//*[@id='comparePageRemoveItemMain_1']/img"); //remove
	
    }
    public void verifySearch()throws Exception
    {
    	click("xpath=//*[@id='SimpleSearchForm_SearchTerm']");
		Thread.sleep(3000);
		sendKeys("xpath=//*[@id='SimpleSearchForm_SearchTerm']","Lenovo");
		Thread.sleep(3000);
		click("xpath=//*[@id='autoSelectOption_4']/div/img");
		Thread.sleep(5000);
		Robot r = new Robot();
		for(int i=0; i<14; i++) {
	        r.keyPress(KeyEvent.VK_PAGE_DOWN);
	    }
		Thread.sleep(4000);
		click("xpath=//*[@id='compare_3074457345616688890']/label"); //dell product
		click("xpath=//*[@id='compare_3074457345616683133']/label"); //lenovo
		click("xpath=//*[@id='compareButton_3074457345616683133']");
		
    }
}
